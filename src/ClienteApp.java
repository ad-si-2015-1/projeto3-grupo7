
import javax.swing.JOptionPane;

public class ClienteApp {

public static void main(String[] args) {
		
		try {
			
			Cliente cliente = new Cliente();
			cliente.setNome(cliente.getResposta("Digite o seu nome: "));
			
			cliente.registrar(cliente.getNome());
			
			IServidor stubServidor = (IServidor) cliente.getRegistry().lookup("Servidor");
			stubServidor.conecta(cliente.getNome());
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro cliente: " + e.toString());
			e.printStackTrace();
		}

	}
	
}
