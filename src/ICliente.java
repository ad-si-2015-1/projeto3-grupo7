

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICliente extends Remote {
	
	void imprime(String msg) throws RemoteException;
	Integer getRespostaMenu(String msg) throws RemoteException;
	String getResposta(String msg) throws RemoteException;
	void terminarExecucao() throws RemoteException;
}
