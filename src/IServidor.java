

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IServidor extends Remote {

	void conecta(String nome) throws RemoteException;
	
}
