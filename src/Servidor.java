
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.AccessException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


/**
 * @author rychard
 *
 */

public class Servidor implements IServidor{
	//Endereco IP do servidor, fixo
	private InetAddress ipServidor;
	//indica a porta usada
	private int porta;
	private ServerSocket servidor;
	// jogadores da partida
	private Jogador[] jogadores;
	// baralho da partida
	private Baralho baralho;
	// campeao da partida
	private Jogador campeao;
	// marcador fim de jogo
	private Boolean partidaFinalizada;
	//numero de jogadores que pararam e esperam por resultado
	private int jogadoresParados;
	//Registro RMI
	private Registry registry;
	//Quantidade de jogadores
	private int qtdJogadores;



	public Servidor(int qtdJogadores){

		this.qtdJogadores = qtdJogadores;
		this.baralho = new Baralho(1,true);
		this.partidaFinalizada = false;
		this.jogadores = new Jogador[getQtdJogadores()];
	}

	public Servidor(Jogador[] jogadores, int porta) throws IOException {
		this.jogadores = jogadores;
		this.porta = porta;
		this.servidor = new ServerSocket(this.getPorta());

		if(jogadores.length < 2){
			//valida��ao numero de jogadores

			// A CRIAR 
		}
		else{		
			this.baralho = new Baralho(1,true);
			this.partidaFinalizada = false;
			iniciar();
		}

	}

	public void registrar(String nome){
		try {
			
			registry = LocateRegistry.createRegistry(1099);
			IServidor stubServidor = (IServidor) UnicastRemoteObject.exportObject(this, 0);
			
			registry.bind(nome, stubServidor);
			
			System.err.println("Server ready");
		} catch (Exception e) {
			System.out.println("Erro de conexão: " + e);
			e.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public void conecta(String nome){

		try {

			ICliente stubCliente = (ICliente) registry.lookup(nome);

			if(jogadores[jogadores.length - 1] == null){

				Jogador jogador = new Jogador();
				jogador.setStubCliente(stubCliente);
				jogador.setNome(nome);
				
				for(int i = 0; i < jogadores.length; i++){
					if(jogadores[i] == null){
						jogadores[i] = jogador;
						i = jogadores.length;
					}
				}
	
				enviarMsg(jogador, "Voce esta conectado " + jogador.getNome() + "!");
				
				if(jogadores[jogadores.length - 1] == null)
					enviarMsg(jogador, "Aguardando conexoes....");
				
				else{
					//Comunica a todos que o jogo vai comecar!
					enviarMsg("O jogo vai comecar em...");

					for(Integer i = 3; i > 0; i--){						
						enviarMsg(i.toString());
						Thread.sleep(1000);
					}

					this.distribuiCartas();	

					fecharConexaoJogadores();
				}
			}
			
			else{
				stubCliente.imprime("Conexao nao aceita, ja existem jogadores suficientes! Por favor aguarde a proxima rodada");
			}
		} catch (AccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void iniciar(){

		while(true){

			System.out.println("Aguardando conexoes....");

			try {

				if (jogadores[0] == null) {

					jogadores[0] = new Jogador();

					jogadores[0].setConexao(servidor.accept());
					enviarMsg(jogadores[0], "Conexao aceita, bem vindo ao jogo!Qual o seu nome?");

					enviarMsg(jogadores[0], "Aguardando demais jogadores, aguarde " + jogadores[0].getNome());
				}

				else if(jogadores[1] == null){

					jogadores[1] = new Jogador();

					jogadores[1].setConexao(servidor.accept());
					enviarMsg(jogadores[1], "Conexao aceita, bem vindo ao jogo!Qual o seu nome?");


					//Comunica a todos que o jogo vai comecar!
					enviarMsg("O jogo vai comecar em...");

					for(Integer i = 3; i > 0; i--){						
						enviarMsg(i.toString());
						Thread.sleep(1000);
					}

					this.distribuiCartas();	

					fecharConexaoJogadores();

				}

				else{
					//olha aquiiiiiiiiiiiiiiiiiiiiiiiiiiii
					Socket s = servidor.accept();
					enviarMsg(jogadores[1], "Conexao nao aceita, ja existem jogadores suficientes! Por favor aguarde a proxima rodada");
					s.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Erro ao tentar conexcao com o Jogador");
			}
		}
	}

	//metodo que distribui cartas para os jogadores e verifica o ganhador
	public void distribuiCartas(){

		//contador de cartas/rodadas
		int k = 0;

		while(!partidaFinalizada && k <= 51){

			for(int i = 0; i < this.jogadores.length; i++){

				// a primeira rodada, apenas distribui uma carta a cada jogador
				if(k < jogadores.length){
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					enviarMsg(jogadores[i], "Sua primeira carta e: " + jogadores[i].getMinhasCartas()[0]);
					k++;
				}

				//Se todos decidiram parar para receber o resultado no final do jogo, então o campeao e informado
				else if(this.jogadoresParados == jogadores.length){

					campeao = new Jogador();
					campeao.setSomaCartas(0);

					for(Jogador j : jogadores){	
						campeao =  campeao.getSomaCartas() <= j.getSomaCartas() && j.getSomaCartas() <= 21 ? j : campeao; 						
					}

					this.partidaFinalizada = true;
					i = this.jogadores.length;
					informaCampeao();

				}

				else{
					// a partir da segunda rodada, conferir se algum jogador esta com 21
					for(int j = 0; j < this.jogadores.length; j++){
						if(this.jogadores[j].getSomaCartas() == 21){
							this.campeao = this.jogadores[j];
							this.partidaFinalizada = true;
							j = this.jogadores.length;
						}

						//verifica se algum jogador estourou (passou de 21), nesse caso ele ja e um perdedor (esta fora da partida)
						else if(this.jogadores[j].getSomaCartas() > 21){
							enviarMsg(jogadores[j], "\n\n" + jogadores[j].getNome() + ", voce estourou a quantidade de pontos, voce esta fora!!!");
							campeao = j == 0 ? jogadores[1] : jogadores[0];
							this.partidaFinalizada = true;
							j = this.jogadores.length;
						}
					}

					if(!partidaFinalizada && !jogadores[i].getParouDeJogar()){

						menu(jogadores[i]);
						enviarMsg(jogadores[i], "\n\nAguarde enquanto os outros jogadores fazem sua jogada");
					}
					//Mostra o vencedor do jogo
					else if(partidaFinalizada){
						i = this.jogadores.length;
						informaCampeao();
					}
				}
			}
		}

	}

	/**
	 * Metodo que envia mensagens informando o vencedor do jogo.
	 * */
	public void informaCampeao(){

		this.enviarMsg(campeao, "Parabens " + campeao.getNome() + ", voce ganhouuuuu! Total de pontos: " + campeao.getSomaCartas());
		this.enviarMsg("O jogador(a) " + campeao.getNome() + " ganhou o jogo!!! Pontos do camepao: " + campeao.getSomaCartas());

	}

	public void fecharConexaoJogadores(){

		enviarMsg("O jogo acabou!!!");

		for(Jogador j : jogadores){
			try {
				Thread.sleep(1000);
				j.getStubCliente().terminarExecucao();
				j = null;
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
		}

	}

	public void menu(Jogador jogador){

		boolean loop = true;

		String opcoes = "Digite uma das opcoes!!\r\n\r\n"
				+ "1-Pegar Carta\r\n"
				+ "2-Parar e esperar ate o final com minha pontuacao\r\n"
				+ "3-Ver minha pontuacao\r\n"
				+ "4-Listar minhas cartas\r\n\r\n"
				+ "Digite o numero da opcao escolhida: ";

		while (loop) {

			switch (lerMsgMenu(jogador, opcoes)) {
			case 1:
				this.addCartas(jogador);
				loop = false;
				break;
			case 2:
				jogador.setParouDeJogar(true);
				this.jogadoresParados++;
				loop = false;
				break;
			case 3:
				enviarMsg(jogador, "Sua pontuacao e: " + jogador.getSomaCartas() + "\n\n");
				break;
			case 4:
				enviarMsg(jogador, listarCartas(jogador) + "\n\n");
				break;
			default:
				enviarMsg(jogador, "Opcao invalida, digite uma correta" + "\n\n");
				break;
			}
		}
	}

	public void addCartas(Jogador jogador){

		jogador.addCartas(baralho.darProximaCarta());
		enviarMsg(jogador, listarCartas(jogador));
		enviarMsg(jogador, "Agora voce tem: " + jogador.getSomaCartas() + " pontos");

	}

	public String listarCartas(Jogador jogador){

		String mensagem = "\nLista de cartas\n";

		for(Cartas carta : jogador.getMinhasCartas()){
			if(carta != null)
				mensagem += carta.toString() + "\n";

		}

		return mensagem;

	}

	public Integer lerMsgMenu(Jogador jogador, String msg){

		try {

			return jogador.getStubCliente().getRespostaMenu(msg);
			
		} catch (Exception e) {
			System.out.println("Erro na leitura da mensagem");
			return null;
		}
	}

	public Boolean enviarMsg(Jogador jogador, String msg){

		String msgEnvio = "\n";
		msgEnvio += msg;

		try {

			jogador.getStubCliente().imprime(msgEnvio);

		} catch (IOException e) {
			return false;
		}
		System.out.println(msgEnvio);

		return true;
	}

	/**
	 * Metodo que envia a mensagem para todos os jogadores
	 */
	public Boolean enviarMsg(String msg){

		String msgEnvio = "\nMensagem para todos os jogadores: ";
		msgEnvio += msg;

		try {

			for(Jogador jogador : jogadores){
			
				if(jogador != null)
					jogador.getStubCliente().imprime(msgEnvio);
			}
		} catch (IOException e) {
			return false;
		}
		System.out.println(msgEnvio);

		return true;
	}

	
	public int getPorta() {
		return porta;
	}

	public void setPorta(int porta) {
		this.porta = porta;
	}

	public ServerSocket getServidor() {
		return servidor;
	}

	public void setServidor(ServerSocket servidor) {
		this.servidor = servidor;
	}

	public int getJogadoresParados() {
		return jogadoresParados;
	}

	public void setJogadoresParados(int jogadoresParados) {
		this.jogadoresParados = jogadoresParados;
	}

	public int getQtdJogadores() {
		return qtdJogadores;
	}

	public void setQtdJogadores(int qtdJogadores) {
		this.qtdJogadores = qtdJogadores;
	}

}
